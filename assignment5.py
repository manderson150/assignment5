#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

import flask

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

##################
## ID Generator ##
##################

# Hopefully 1000 is enough test IDs...
idList = [x for x in range(4,1000)]
idFactory = (y for y in idList)

####################
## Helper Methods ##
####################

def findBook(bookId):
    for book in books:
        if int(book['id']) == bookId:
            return book

def removeBook(bookId):
    global books
    for idx, book in enumerate(books):
        if int(book['id']) == bookId:
            del books[idx]
            return

#####################
## Routing Methods ##
#####################

@app.route('/book/JSON/')
def bookJSON():
    return flask.jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('show.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method  == 'GET':
        return render_template('newBook.html')
    elif request.method == 'POST':
        # Grab ID from factory
        books.append({'title': request.form['bookName'], 'id': next(idFactory)})
        return redirect(url_for('showBook'))

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'GET':
        return render_template('editBook.html', book=findBook(book_id))
    elif request.method == 'POST':
        workingBook = findBook(book_id)
        workingBook['title'] = request.form['bookName']
        return redirect(url_for('showBook'))
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'GET':
        return render_template('deleteBook.html', book=findBook(book_id))
    elif request.method == 'POST':
        # Add ID back to factory
        removeBook(book_id)
        global idList
        idList += [int(book_id)]
        return redirect(url_for('showBook'))

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

